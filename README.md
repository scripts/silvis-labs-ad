# Silvis Labs AD PowerShell Scripting Library

Powershell scripts for Active Directory usage in Silvis Labs

# Installation
Using the scripting library to create a home directory can be done on any Windows client as long as it has network access to the file share.   However, the creation of quotas MUST be done locally on the machine itself (via the console or RDP).   The installation locations are each machine is the same, but keep in mind that the creation of quotas will only work when on the server itself.

Also, in order to do quotas on a file server you must install the **File Server Resource Manager** under *File and Storage Services -> File and iSCSI Services* in the Windows Server Manager.

1. [Download](https://git.aae.wisc.edu/scripts/silvis-labs-ad/-/archive/main/silvis-labs-ad-main.zip?path=SilvisLabsModules) the SilvisLabsModules folder.
2. For easiest usage, place the `SilvisLabsModules` folder in your personal PowerShell modules folder .
   - For Windows PowerShell (default install) - `C:\users\<username>\Documents\WindowsPowershell\Modules`
   - For Windows PowerShell 7 - `C:\users\<username>\Documents\Powershell\Modules`
3. Navigate to the `SilvisLabsModules`, right click on each file, select **Properties**, then check **Unblock**.  Click OK.

# Usage

1. Open Powershell with the **Run as Administrator** option.
2. Type in the following `runas` command to open another Powershell session with your -OU/-SA account.

   ```powershell
   # For Windows Powershell (default install)
   runas /netonly /noprofile /user:<netid>-ou@ad.wisc.edu powershell
 
   # If using Powershell 7
   runas /netonly /noprofile /user:<netid>-ou@ad.wisc.edu pwsh
   ```
3. In the new Powershell window type:
   ```powershell
   Import-Module SilvisLabsModules -Force
   ```
Now you're all set to use the scripting library.

# Add-HomeAndQuota
Uses the `Add-HomeDirectory`, `Add-NetIdPermission`, and `New-Quota` scripts together in a single script.   Creates a home directory, adds a user's NetID with Modify permissions to the new directory, and creates a quota on the home directory

## Syntax
```powershell
Add-HomeAndQuota
    [-ParentDir] <String>
    [-NetId] <String>
    [-Size] <UInt64>
    [-Overwrite]
```

## Parameters
**-ParentDir**
  - Folder in which to create the new home directory

**-NetId**
  - NetId for the home directory.  Used for folder naming and permissions application
 
 **-Size**
   - Specifies the space limit that the quota template enforces.  Use format like 5GB, 1TB, etc

**-Overwrite**
   - Overwrites an existing quota (if it exists) on the folder

## Examples
```powershell
# Add a folder C:\home\jdoe with Modify permissions for jdoe and a 5GB quota
Add-HomeAndQuota -ParentDir "C:\home" -NetId "jdoe" -Size 5GB

# Use a CSV file to import users.  Columns must match the parameter names (ParentDir, NetId, etc).   
# Size must be specified on the command line - unable to import from CSV
Import-CSV "C:\location\to\file.csv" | Add-HomeAndQuota -Size 5GB

# Use a CSV file to import users.  Columns must match the parameter names (ParentDir, NetId, etc).   
# If all folders use the same parent directory, it can be specified in the command line instead of the CSV file.
Import-CSV "C:\location\to\file.csv" | Add-HomeAndQuota -Size 5GB -ParentDir "C:\home"
```

# Add-HomeDirectory
Uses the `Add-NetIdPermission` script to creates a home directory and add a user's NetID with Modify permissions to the new directory.

## Syntax
```powershell
Add-HomeDirectory
    [-ParentDir] <String>
    [-NetId] <String>
```

## Parameters
**-ParentDir**
  - Folder in which to create the new home directory

**-NetId**
  - NetId for the home directory.  Used for folder naming and permissions application
 
## Examples
```powershell
# Add a folder C:\home\jdoe with Modify permissions for jdoe
Add-HomeDirectory -ParentDir "C:\home" -NetId "jdoe"

# Use a CSV file to import users.  Columns must match the parameter names (ParentDir, NetId, etc).   
Import-CSV "C:\location\to\file.csv" | Add-HomeDirectory

# Use a CSV file to import users.  Columns must match the parameter names (ParentDir, NetId, etc).   
# If all folders use the same parent directory, it can be specified in the command line instead of the CSV file.
Import-CSV "C:\location\to\file.csv" | Add-HomeDirectory -ParentDir "C:\home"
```

# Add-NetIdPermission
Sets a permission on a folder for a single NetId

## Syntax
```powershell
Add-NetIdPermission
    [-Path] <String>
    [-NetId] <String>
    [-Rights] <String>
```

## Parameters
**-Path**
  - Specifies a valid local path to a folder

**-NetId**
  - NetId for the home directory.  Used for folder naming and permissions application
 
**-Rights**
  - Specifies the rights to assign for the NetId.  Possible values are ListDirectory, ReadData, WriteData, CreateFiles, CreateDirectories, AppendData, ReadExtendedAttributes, WriteExtendedAttributes, Traverse, ExecuteFile, DeleteSubdirectoriesAndFiles, ReadAttributes, WriteAttributes, Write, Delete, ReadPermissions, Read, ReadAndExecute, Modify, ChangePermissions, TakeOwnership, Synchronize, FullControl

## Example
```powershell
# Add a folder C:\home\jdoe with Modify permissions for jdoe
Add-NetIdPermission -Path "C:\home" -NetId "jdoe" -Rights "Modify"
```

# New-Quota
Creates a quota on the directory

## Syntax
```powershell
New-Quota
    [-Path] <String>
    [-Size] <UInt64>
    [-Overwrite]
```

## Parameters
**-Path**
  - Specifies a valid local path to a folder.

 **-Size**
   - Specifies the space limit that the quota template enforces.  Use format like 5GB, 1TB, etc

**-Overwrite**
   - Overwrites an existing quota (if it exists) on the folder

## Examples
```powershell
# Create a quota on C:\home\jdoe for 5GB, overwriting an existing quota if exists
New-Quota -Path "C:\home" -Size 5GB -Overwrite

# Use a CSV file to batch create quotas.  Column must match the parameter name (Path).  
# Size must be specified on the command line - unable to import from CSV
Import-CSV "C:\location\to\file.csv" | New-Quota -Size 5GB
```
