<#
.SYNOPSIS
    Creates a home directory

.DESCRIPTION
    Creates a folder in the specified parent directory with the NetID name, with NetID Modify permissions

.PARAMETER ParentDir
    Folder in which to create the new home directory

.PARAMETER NetId
    NetId for the home directory.  Used for folder naming and permissions application

.OUTPUTS
    The home directory full path

.EXAMPLE
    Adds the home directory and sets Modify permissions

    $HomeDirectory = Add-HomeDirectory "C:\home" "jdoe"
#>
function Add-HomeDirectory {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$ParentDir,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$NetId        
    )    

    Begin {
        $RightModify="Modify"
    }

    Process {
        $HomeDir = Join-Path $ParentDir $NetId

        # test for folder existence - create if not found
        if (-Not (Test-Path -Path $HomeDir)) {
            Write-Information "$($HomeDir) does not exist. Creating."
            $null = New-Item -Path $ParentDir -Name $NetID -ItemType Directory
            Write-Information "   $($HomeDir) created."
        }

        Add-NetIdPermission $HomeDir $NetId $RightModify

        return $HomeDir
    }
    End {

    }

    
}


<#
.SYNOPSIS
    Sets a permission on a folder for a single NetId

.PARAMETER Path
    Specifies a valid local path to a folder.

.PARAMETER NetId
    NetId for permissions application

.PARAMETER Rights
    Specifies the rights to assign for the NetId.  Possible values are ListDirectory, ReadData, WriteData, CreateFiles, CreateDirectories, AppendData, ReadExtendedAttributes, WriteExtendedAttributes, Traverse, ExecuteFile, DeleteSubdirectoriesAndFiles, ReadAttributes, WriteAttributes, Write, Delete, ReadPermissions, Read, ReadAndExecute, Modify, ChangePermissions, TakeOwnership, Synchronize, FullControl
#>
function Add-NetIdPermission 
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$Path,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$NetId,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$Rights                      
    )   

    Begin {
        $InheritanceFlag = [System.Security.AccessControl.InheritanceFlags]::ContainerInherit -bor [System.Security.AccessControl.InheritanceFlags]::ObjectInherit
        $PropagationFlag = [System.Security.AccessControl.PropagationFlags]::None
        $objType = [System.Security.AccessControl.AccessControlType]::Allow 
    }    
    Process {
        # apply permissions
        Write-Information "Setting $($Rights) permission for AD\$($NetId)."

        Try {
            $Acl=Get-Acl $Path

            #define a new access rule.
            $Permission = "AD\$($NetId)", $Rights, $InheritanceFlag, $PropagationFlag, $objType
            $Rule=New-Object System.Security.AccessControl.FileSystemAccessRule $Permission

            #Add this access rule to the ACL
            $Acl.SetAccessRule($Rule)

            #Write the changes to the object
            Set-Acl $Path $Acl

            Write-Information "   Permission applied."
        }
        Catch {
            Write-Error "Error: $($_.Exception.Message)" 
        }    
    }           
}

<#
.SYNOPSIS
    Creates a new quota on the specified folder

.PARAMETER Path
    Specifies a valid local path to a folder.

.PARAMETER Size
    Specifies the space limit that the quota template enforces.

.PARAMETER Overwrite
    Specifies whether to overwrite an existing quota, if one exists.

#>
function New-Quota 
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]$Path,
        [UInt64]$Size,
        [switch]$Overwrite
    )        

    if ($PSBoundParameters.ContainsKey('Size') -ne $True) {
        Write-Error -Message "The size of the quota was not specified"
    }
    
    $null = New-FsrmQuota -Path $Path -Size $Size -ErrorVariable StupidError -ErrorAction SilentlyContinue
    if ($StupidError.Count -gt 0 -and $StupidError[0].CategoryInfo.Category -eq "ResourceExists") {
        if ($Overwrite.IsPresent) {
            Remove-FsrmQuota -Path $Path -Confirm:$false
            $null = New-FsrmQuota -Path $Path -Size $Size 
        }
        else {
            Write-Warning "Quota already exists on $($Path).  Use -Overwrite `$true to override."
        }
    }
    
    
}

<#
.SYNOPSIS
    Creates a home directory with permissions and quota on it.

.DESCRIPTION
    Creates a folder in the specified parent directory with the NetID name, with NetID Modify permissions.  Sets a quota with specified size.

.PARAMETER ParentDir
    Folder in which to create the new home directory

.PARAMETER NetId
    NetId for the home directory.  Used for folder naming and permissions application

.PARAMETER Size
    Specifies the space limit that the quota template enforces.

.OUTPUTS
    The home directory full path

.EXAMPLE
    Adds the home directory and sets Modify permissions

    $HomeDirectory = Add-HomeAndQuota "C:\home" "jdoe" 5GB -Overwrite
#>

function Add-HomeAndQuota
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$ParentDir,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$NetId,
        [Parameter(ValueFromPipelineByPropertyName=$true)]
        [UInt64]$Size,
        [switch]$Overwrite                
    )        

    Process {
        if ($PSBoundParameters.ContainsKey('Size') -ne $True) {
            Write-Error -Message "The size of the quota was not specified"
        }

        $HomeDir = Add-HomeDirectory -ParentDir $ParentDir -NetId $NetId
        New-Quota -Path $HomeDir -Size $Size -Overwrite:$Overwrite
    }
    
}
